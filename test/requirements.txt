boto3==1.17.*
pytest==4.3.0
requests==2.25.*
bitbucket-pipes-toolkit==2.1.0
